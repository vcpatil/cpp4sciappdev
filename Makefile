OBJS = main.o student.o 
CFLAGS = -Wall -g
SRC_PATH = src 
INC = include

cppsciappdev : $(OBJS)
	g++ -o cppsciappdev $(OBJS)

$(OBJS): $(INC)/*.h

main.o: $(SRC_PATH)/main.cpp
	g++ -c $(CFLAGS) -I$(INC) $(SRC_PATH)/main.cpp
	
student.o: $(SRC_PATH)/student.cpp
	g++ -c $(CFLAGS) -I$(INC) $(SRC_PATH)/student.cpp

test : 
	cppsciappdev

.PHONY: 
clean:
	rm $(OBJS) cppsciappdev 
	